<?php require('../template.php'); pageHeader(); ?>

<!--
  Changes to this file should be committed also to
  https://gitlab.com/sosy-lab/test-comp/bench-defs/-/blob/main/doc/rules.php
-->

<style type="text/css">
  table, td  {border: 1px solid grey;}
</style>

<h2>Call for Participation &mdash; Procedure</h2>

<p>
The competition compares state-of-the-art tools for 
automatic software testing with respect to effectiveness and efficiency.
The competition consists of two phases:
a training phase, in which benchmark programs are given to the tool developers,
and an evaluation phase, in which all participating test-generation tools will be executed on 
benchmark test tasks, and the number of found bugs, the coverage, as well as the runtime is measured.
The competition is performed and presented by the Test-Comp competition chair. 
</p>

<p>
This page is the authoritative reference for the rules of the competition.
Please also read the <a href="../faq.txt">FAQ with more descriptions</a>.
</p>


<h3>Publication and Presentation of the Competition Candidates</h3>

<p>
The jury selects qualified competition candidates to publish (in the FASE conference proceedings)
a contribution paper that gives an overview of the competition candidate
(the selection criteria is not novelty but text quality in terms of describing the tool
 and the qualification of the tool).
An overview paper by the competition organizers will describe the competition procedure
and present the results.
The paper-submission deadline is specified on the <a href="dates.php">dates</a> page;
submission requirements are explained on the <a href="submission.php">submission page</a>.
</p>

<p>
In addition, every qualified competition candidate is granted a presentation slot
in the FASE program to present the competition candidate to the ETAPS audience.
</p>


<h3>Definition of Test Task</h3>

<p>
A <i>test task</i> consists of a C program and a test specification.
A <i>test run</i> is a non-interactive execution of a competition candidate on a single test task,
in order to generate a result value and test suite according to the test specification.
</p>

<p>
The <i>result</i> of a test run is a triple (ANSWER, TEST-SUITE, TIME).
ANSWER has the value <span style="color:green">DONE</span> if the tool
finished to compute a TEST-SUITE that tries to achieve the coverage goal
as defined in the test specification,
and other values (such as <span style="color:orange">UNKNOWN</span>)
if the tool terminates prematurely by a tool crash, time-out, or out-of-memory.
</p>

<p>
TEST-SUITE is a directory of files according to the
<a href="https://gitlab.com/sosy-lab/test-comp/test-format/blob/testcomp23/doc/Format.md">format for exchangeable test-suites</a> (test-suite format).
Irrespectively of the ANSWER, the (partial) TEST-SUITE is validated.
</p>

<p>
TIME is the consumed CPU time until the tester terminates or is terminated.
It includes the consumed CPU time of all processes that the tester starts.
If TIME is equal to or larger than the time limit, then the tester
is terminated.
</p>

<p>
The C programs are partitioned into categories,
which are defined in category-set files.
The categories and the contained programs 
are explained on the <a href="benchmarks.php">benchmark page</a>.
The requirements for the C programs are described <a href="#programs">below</a>.
</p>


<h4>Test Specifications</h4>

<p>
The specifications for testing a program are given
in files <tt>properties/coverage-error-call.prp</tt> and <tt>properties/coverage-branches.prp</tt>.
</p>

<p>
The definition 'init(main())' gives the initial states of the program by a call of function 'main'.
  If the program defines the function main with parameters (as allowed by the C standard),
  we assume that the initial values of these parameters satisfy the conditions specified in the C standard (detailed in Section 5.1.2.2.1, "Program Startup", of the C99 standard).
</p>
<p>
The definition <tt>FQL(f)</tt> specifies that coverage definition <tt>f</tt> should be achieved.
The FQL (FShell query language) coverage definition <tt>COVER EDGES(@DECISIONEDGE)</tt> means that all branches should be covered,
<tt>COVER EDGES(@BASICBLOCKENTRY)</tt> means that all statements should be covered,
and <tt>COVER EDGES(@CALL(__VERIFIER_error))</tt> means that function <tt>__VERIFIER_error</tt> should be called
[<a href="https://forsyte.at/software/fshell/fql/">lang def</a>].
</p>


<h5>Bug Finding:</h5>
<p>
    <pre><?php echo trim(file_get_contents("https://test-comp.sosy-lab.org/2023/results/sv-benchmarks/c/properties/coverage-error-call.prp")); ?>
    </pre>
</p>

<table>
<thead>
<tr>
<td>Formula</td><td>Definition</td>
</tr>
</thead>
<tbody>
<tr>
  <td style="width: 150px;" ><pre>COVER EDGES(@CALL(func))</pre></td>
  <td>
    The test suite contains at least one test that executes function <tt>func</tt>.
</td>
</tr>
</tbody>
</table>


<h5>Coverage:</h5>
<p>
    <pre><?php echo trim(file_get_contents("https://test-comp.sosy-lab.org/2023/results/sv-benchmarks/c/properties/coverage-branches.prp")); ?>
    </pre>
</p>

<table>
<thead>
<tr>
<td>Formula</td><td>Definition</td>
</tr>
</thead>
<tbody>
<tr>
  <td style="width: 150px;" ><pre>COVER EDGES(@DECISIONEDGE)</pre></td>
  <td>
    The test suite contains tests such that all branches of the program are executed.
</td>
</tr>
</tbody>
</table>



<h3 id="scores">Evaluation by Scores and Runtime</h3>

<h4>Finding Bugs</h4>
<p>
The first category is to show the abilities to discover bugs.
The programs in the benchmark set contain programs that contain a bug.
</p>

<em>Evaluation by scores and runtime</em>
<p>
Every run will be started by a batch script,
which produces for every tool and every test task (a C program) 
one of the following score and result:
</p>
<table>
 <tbody>
  <tr>
    <td width="100px">+1 point</td>
    <td>for a generated TEST-SUITE that contains a test witnessing the specification violation
        and TIME is less than the time limit
    </td>
  </tr>
  <tr>
    <td>0 points</td><td>otherwise</td>
  </tr>
 </tbody>
</table>

<p>
The participating test tools are ranked according to the sum of points.
Tools with the same sum of points are ranked according to success-runtime.
The success-runtime for a tool is the total CPU time over all
test tasks for which the tool produced a test suite that revealed the bug.
</p>


<h4>Code Coverage</h4>
<p>
The second category is to cover as many program branches as possible.
The coverage criterion was chosen because many test-generation tools support this standard criterion by default.
Other coverage criteria can be reduced to branch coverage by transformation<!--~\cite{TestabilityTransformation}-->.
</p>

<em>Evaluation by scores and runtime</em>
<p>
Every run will be started by a batch script,
which produces for every tool and every test task (a C program) 
the coverage (as reported by <tt><a href="https://gitlab.com/sosy-lab/software/test-suite-validator">TestCov</a></tt>
[<a href="https://www.sosy-lab.org/research/pub/2019-ASE.TestCov_Robust_Test-Suite_Execution_and_Coverage_Measurement.pdf"><small>ASE'19</small></a>]; a value between <tt>0</tt> and <tt>1</tt>) of branches of the program that are covered by the generated test cases.
</p>
<table>
 <tbody>
  <tr>
    <td width="100px">+<tt>cov</tt> points</td>
    <td>for a generated TEST-SUITE that yields coverage <tt>cov</tt>
        and TIME is less than the time limit
    </td>
  </tr>
  <tr>
    <td>0 points</td><td>otherwise</td>
  </tr>
 </tbody>
</table>

<p>
The participating test tools are ranked according to the accumulated coverage.
Tools with the same coverage are ranked according to success-runtime.
The success-runtime for a tool is the total CPU time over all
test tasks for which the tool reported a test suite.
</p>

<h4>Runtime</h4>
<p>
Runtime is the consumed CPU time until the tester terminates.
It includes the consumed CPU time of all processes that the tester starts.
If TIME is equal to or larger than the time limit, then the tester
is terminated.
</p>




<h3 id="programs">Benchmark Test Tasks</h3>


<p>
The training test tasks will be provided by a <a href="dates.php">specified date</a> on <a href="benchmarks.php">this web page</a>.
A subset of the training test tasks will be used for the actual competition experiments.
The jury agrees on a procedure to define the subset (if not all test tasks are taken).
We avoid using test tasks without training, because the semantics of the language C is underspecified;
the participants, jury, and community ensure that all involved parties agree on the intended meaning of the test tasks.
(This is in contrast to competitions over completely specified problems, such as SAT or SMT.)
</p>

<p>
The programs are assumed to be written in GNU C (some of them adhere to ANSI C).
Each program contains all code that is needed for the testing, i.e., no includes (cpp -P).
Some programs are provided in CIL (C Intermediate Language) <a href="http://www.eecs.berkeley.edu/~necula/cil/">[1]</a>.
</p>

<p>
Potential competition participants are invited to submit benchmark test tasks until the specified date.
Test tasks have to fulfill two requirements, to be eligible for the competition:
(1) the program has to be written in GNU C or ANSI C, and
(2) the program has to come with a specification given by one of the properties stated above.
Other specifications are possible, but need to be proposed and discussed.
</p>

<p>
New proposed categories will be included if at least three different tools or teams participate in the category
(i.e., not the same tool twice with a different configuration).
</p>

<p>
For each category, we specify whether the programs are written for 
an <strong>ILP32 (32-bit)</strong> or an <strong>LP64 (64-bit)</strong> architecture
(cf. <a href="http://www.unix.org/whitepapers/64bit.html">http://www.unix.org/whitepapers/64bit.html</a>).
</p>

<p>
In the following, we list a few conventions that are used in some of the test tasks,
in order to express special information that is difficult to capture with the C language.
</p>

<p>
<span style="text-decoration: line-through;">
<strong>__VERIFIER_error(): </strong>
For checking reachability we use the function <tt>__VERIFIER_error()</tt>.
The verification tool can assume the following implementation:<br>
<tt>void __VERIFIER_error() { abort(); }</tt><br>
Hence, a function call <tt>__VERIFIER_error()</tt> never returns and in the function <tt>__VERIFIER_error()</tt> the program terminates.
</span>
</p>

<p>
<span style="text-decoration: line-through;">
<strong>__VERIFIER_assume(expression): </strong>
A verification tool can assume that a function call
<tt>__VERIFIER_assume(expression)</tt>
has the following meaning: If 'expression' is evaluated to '0', then the function loops forever,
otherwise the function returns (no side effects).
The verification tool can assume the following implementation:<br/>
<tt>void __VERIFIER_assume(int expression) { if (!expression) { LOOP: goto LOOP; }; return; }</tt>
</span>
</p>

<p>
<strong>__VERIFIER_nondet_X(): </strong>
In order to model nondeterministic values, the following functions can be assumed to return
an arbitrary value of the indicated type:
<tt>__VERIFIER_nondet_X()</tt>
with <tt>X</tt> in {<tt>bool</tt>, <tt>char</tt>, <tt>int</tt>, <tt>int128</tt>, <tt>float</tt>, <tt>double</tt>, <tt>loff_t</tt>, <tt>long</tt>, <tt>longlong</tt>, 
<tt>pchar</tt>, <tt>pthread_t</tt>, <tt>sector_t</tt>, <tt>short</tt>, <tt>size_t</tt>, <tt>u32</tt>, 
<tt>uchar</tt>, <tt>uint</tt>, <tt>uint128</tt>, <tt>ulong</tt>, <tt>ulonglong</tt>, <tt>unsigned</tt>, <tt>ushort</tt>} 
(no side effects, <tt>pointer</tt> for <tt>void *</tt>, etc.).
The test tool can assume that the functions are implemented according to the following template:<br/>
<tt>X __VERIFIER_nondet_X() { X val; return val; }</tt>
</p>

<p>
<strong>__VERIFIER_atomic_begin(): </strong>
For modeling an atomic execution of a sequence of statements in a multi-threaded run-time environment,
those statements can be placed between two function calls <tt>__VERIFIER_atomic_begin()</tt> and <tt>__VERIFIER_atomic_end()</tt>
(deprecated but still valid: those statements can be placed in a function whose name matches <tt>__VERIFIER_atomic_</tt>).
The testers are instructed to assume that the execution between those calls is not interrupted.
The two calls need to occur within the same control-flow block; nesting or interleaving of those function calls is not allowed.
</p>

<p>
<strong>malloc(), free(): </strong>
We assume that the functions <tt>malloc</tt> and <tt>alloca</tt> always return
a valid pointer, i.e., the memory allocation never fails,
and function <tt>free</tt> always deallocates the memory and 
makes the pointer invalid for further dereferences.
</p>

<h3 id="setup">Competition Environment and Requirements</h3>

<h4>Competition Environment</h4>

<p>
Each test run will be started on a machine with
a GNU/Linux operating system (x86_64-linux, the latest LTS release of Ubuntu);
there are three resource limits for each test run: 
a memory limit of 15 GB (14.6 GiB) of RAM,
a runtime limit of 15 min of CPU time,
and a limit to 4 processing units of a CPU.
Further technical parameters of the competition machines are available in the
<a href="https://gitlab.com/sosy-lab/test-comp/bench-defs/">repository that also contain the benchmark definitions</a>.
</p>


<h4 id="parameters">Benchmark Definition</h4>

<p>
The competition environment feeds the candidates with parameters when started for a test run.
There is one global set of parameters that can be used to tune the tester to the benchmark programs.
Testers are forbidden from using the program name, its hash, or the current category to tune their parameters.
This set of (command-line) parameters have to be defined in the competition contribution paper
and in the benchmark definition.
One parameter defines the specification file.
There are categories that need to be tested with the information that a 64-bit architecture is used,
thus, if the tester has a parameter to handle this aspect, it needs to be defined.
</p>

<p>
In order to participate at Test-Comp, a benchmark definition in the Test-Comp repository is necessary.
Technically, the benchmark definition needs to be integrated into the Test-Comp repository
under directory <a href="https://gitlab.com/sosy-lab/test-comp/bench-defs/tree/main/benchmark-defs">benchmark-defs</a>
using a <a href="https://gitlab.com/sosy-lab/test-comp/bench-defs/merge_requests">merge request</a>.
</p>

<p>
The benchmark definition defines the categories that the tester is to be executed on,
the parameters to be passed, and the resource limits.
</p>

<h4>Tool-Info Module</h4>

<p>
In order to participate at Test-Comp, a tool-info module in the BenchExec repository is necessary.
Technically, the tool-info module needs to be integrated into the BenchExec repository
under directory <a href="https://github.com/sosy-lab/benchexec/tree/main/benchexec/tools">benchexec/tools</a>
using a <a href="https://github.com/sosy-lab/benchexec/pulls">pull request</a>.
</p>

<p>
The task of the tool-info module is (besides other tasks) to translate the output of a tester to uniform results.
For running the contributed tester, the organizer follows the installation requirements and executes
the tester, relying on the tool-info module for correct execution of the tester and correct interpretation of its results.
</p>

<p>
The tool-info module must be tested, cf. 
<a href="https://github.com/sosy-lab/benchexec/blob/main/doc/tool-integration.md#testing-the-tool-integration">instructions on tool integration</a>.
</p>



<h4 id="tester">Tester</h4>

The submitted system has to meet the following requirements:
<ul>
  <li>The tester is publicly available for download and the license
      (i)   allows reproduction and evaluation by anybody,
      (ii)  does not place any restriction on the test output (log files, witnesses), and
      (iii) allows (re-) distribution of the unmodified tester archive
            for reproduction via the archives repository and Zenodo.
  </li>
  <li>The archive of the tester contains a LICENSE file that satisfies the above requirements.
  </li>
  <li>The archive of the tester contains a README file that describes the contents.
  </li>
  <li>The tester is archived in a ZIP file (.zip), which contains exactly one directory (no tarbomb).
  </li>
  <li>The tester does not exceed an stdout/stderr limit of at most 2 MB.
  </li>
  <li>The archive does not contain large amounts of unnecessary data, such as
      repository data (.svn), source files, aux folders like __MACOSX, and test files.
  </li>
  <li>The tester should not require any special software on the competition machines; all necessary libraries and external tools
      should be contained in the archive.  Standard packages that are available as Ubuntu packages can be requested.
  </li>
</ul>




<h3 id="qualification">Qualification</h3>

<p>
<b>Tester.</b> A test tool is qualified to participate as competition candidate if the tool is 
(a) publicly available for download and fulfills the <a href="#tester">above license requirements</a>,
(b) works on the GNU/Linux platform (more specifically, it must run on an x86_64 machine with the latest Ubuntu LTS),
(c) is installable with user privileges (no root access required, except for required standard Ubuntu packages)
    and without hard-coded absolute paths for access to libraries and non-standard external tools,
(d) succeeds for more than 50 % of all training programs to parse the input and 
    start the test process (a tool crash during the test phase does not disqualify), and
(e) produces test suites that adhere to the
    <a href="https://gitlab.com/sosy-lab/test-comp/test-format/blob/testcomp23/doc/Format.md">exchange format</a>
    (syntactically correct).
</p>
<p>
<b>Person.</b> A person (participant) is qualified as competition contributor for a competition candidate if
the person (a) is a contributing designer/developer 
of the submitted competition candidate
(witnessed by occurrence of the person’s name on the tool's project web page, a tool paper,
or in the revision logs)
or (b) is authorized by the competition organizer (after the designer/developer was contacted about the participation).
</p>
<p>
<b>Paper.</b> A paper is qualified if the quality of the description of the competition candidate suffices to run the tool in the competition
and meets the scientific standards of the venue (conference or journal)
as competition-candidate representation in the conference proceedings or journal issue.
</p>

<p>
Note: A test tool can participate several times as different competition candidates, if a significant difference of the
conceptual or technological basis of the implementation is justified in the accompanying description paper.
This applies to different versions as well as different configurations,
in order to avoid forcing developers to create a new tool for every new concept.
</p>


<p>
The participants have the opportunity to check the tester results against their own expected results
and discuss inconsistencies with the competition chair.
</p>


<h3>Opting-out from Categories</h3>

Every team can submit for every category 
(including meta categories, i.e., categories that consist of test tasks from other categories)
an opt-out statement.
In the results table, a dash is entered for that category; no execution results are reported in that category.

If a team participates (i.e., does not opt-out), *all* test tasks that belong to that category
are executed with the tester.  
The obtained results are reported in the results table;
the scores for meta categories are weighted according to the established procedure.
(This means, a tool can participate in a meta category and at the same time opt-out from a sub-category,
 with having the real results of the tool counted towards the meta category, but not reported for the sub-category.)


<h3 id="meta" >Computation of Scores for Meta Categories</h3>

<p>
A meta category is a category that consists of several sub-categories.
The scores in such a meta category is computed from the normalized scores in its sub-categories.
</p>

<h4>Procedure</h4>

<p>
The scores for a (meta) category
is computed from the scores of all k contained (sub-) categories
using a normalization by the number of contained test tasks:
The normalized score sn_i of a tester in category i is obtained by dividing the score s_i by the number of tasks n_i in category i
(sn_i = s_i / n_i),
then the sum st = SUM(sn_1, ..., sn_k) over the normalized scores of the categories
is multiplied by the average number of tasks per category.
</p>

<h4>Motivation</h4>

<p>
The goal is to reduce the influence of a test task in a large category compared to a test task
in a small category, and thus, balance over the categories.
<!--
Normalizing by score is not an option because we assigned higher positive scores for TRUE
and higher negative scores for wrong results. (Normalizing by score would remove those desired differences.)
</p>

<h4>Example</h4>

<p>
Category 1 has 10 test tasks with result TRUE.<br />
Category 2 has 10 test tasks with result FALSE.
</p>

<p>
Tester A correctly solved 5 test tasks in category 1 -> 10 points, and 5 test tasks in category 2 -> 5 points.<br />
Overall score of Tester A: (( (10/10 = 1)  +  (5/10 = 0.5) ) = 1.5)  *  (10 + 10 = 20) / 2 = 15
</p>

<p>
Tester B correctly solved 10 test tasks in category 1 -> 20 points, and 0 test tasks in category 2 -> 0 points.<br />
Overall score of Tester B: (( (20/10 = 2)  +  (0/10 = 0.0) ) = 2.0)  *  (10 + 10 = 20) / 2 = 20
</p>

<p>
Tester C correctly solved 0 test tasks in category 1 -> 0 points, and 10 test tasks in category 2 -> 10 points.<br />
Overall score of Tester C: (( (0/10 = 0)  +  (10/10 = 1.0) ) = 1.0)  *  (10 + 10 = 20) / 2 = 10
<p>

<p>
Tester D correctly solved 8 test tasks in category 1 -> 16 points, and 8 test tasks in category 2 -> 8 points.<br />
Overall score of Tester D: (( (16/10 = 1.6)  +  (8/10 = 0.8) ) = 2.4)  *  (10 + 10 = 20) / 2 = 24
</p>

<p>
Obtained ranking:<br />
1. Tester D<br />
2. Tester B<br />
3. Tester A<br />
4. Tester C
</p>

<p>
Tester D was strong in all categories, and thus, won cotagory Overall.<br />
Tester B was even stronger than Tester D in one category, but performed not so well in the other category, and thus, only got second overall.
</p>
-->

<p style="text-align: right; font-size: 70%;">
<?php echo "Last changed: " . date("c", filemtime("rules.php")); ?>
</p>

<?php pageFooter(); ?>
